<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Message;
use App\Repository\GroupRepository;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class GroupController
 * @package App\Controller
 * @Route("/api")
 */
class GroupController extends AbstractFOSRestController
{
    private $entityManager;
    private $groupRepository;
    private $messageRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        GroupRepository $groupRepository,
        MessageRepository $messageRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->groupRepository = $groupRepository;
        $this->messageRepository = $messageRepository;
    }

    /**
     * @Rest\Get("/groups", name="find_groups")
     * @Rest\QueryParam(name="name", description="Keyword to search with", nullable=false)
     * @param Rest\QueryParam $queryParam
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function findGroups(ParamFetcher $paramFetcher)
    {
        $MAX_RESULTS_LIMIT = 10;
        $name = $paramFetcher->get('name');

        if(trim($name)) {
            $groups = $this->groupRepository->createQueryBuilder('g')
                ->where('g.name LIKE :name')
                ->setMaxResults($MAX_RESULTS_LIMIT)
                ->orderBy('g.createdAt', 'DESC')
                ->setParameter('name', "%$name%")
                ->getQuery()->getResult();

            return $this->json(["data" => $groups], Response::HTTP_OK);
        }

        return $this->json(['message' => 'Invalid parameters'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Post("/groups", name="create_group")
     * @Rest\RequestParam(name="name", description="Group name", nullable=false)
     * @Rest\RequestParam(name="members", description="Group members", nullable=true)
     * @param ParamFetcher $paramFetcher
     */
    public function createGroup(ParamFetcher $paramFetcher)
    {
        $name = $paramFetcher->get('name');
        $members = $paramFetcher->get('members');

        if(trim($name)) {
            $group = new Group();
            $group->setName($name);
            $group->setCreatedAt(new \DateTime());
            // ToDo add members from $members if exist
            $this->entityManager->persist($group);
            $this->entityManager->flush();

            return $this->json([
                'success' => true
            ], Response::HTTP_CREATED);
        }

        return $this->json([
            'message' => 'Invalid parameters'
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Patch("/group/{id}", name="update_group")
     * @Rest\RequestParam(name="name", description="New name for group", nullable=true)
     * @param $id
     * @param ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function updateGroup($id, ParamFetcher $paramFetcher)
    {
        $id = (int) $id;
        $name = $paramFetcher->get('name');

        // ToDo Check if current user is owner of group
        if($id > 0 && in_array(true, [$name])) {
            $group = $this->groupRepository->find($id);
            if($name) $group->setName($name);
            $this->entityManager->persist($group);
            $this->entityManager->flush();

            return $this->json([
                'success' => true
            ], Response::HTTP_OK);
        }

        return $this->json([
            'message' => 'Invalid parameters'
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Delete("/group/{id}", name="delete_group")
     */
    public function deleteGroup($id)
    {
        $id = (int) $id;
        if($id) {
            $group = $this->groupRepository->find($id);

            // ToDo Check if current user is owner of group
            if ($group) {
                $this->entityManager->remove($group);

                $this->entityManager->flush();

                return $this->json([
                    'success' => true
                ],Response::HTTP_OK);
            }
        }

        return $this->json([
            'message' => 'Invalid parameters'
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Get("/group/{id}/members", name="get_group_members")
     */
    public function getGroupMembers()
    {
        // ToDo Check if current user is member of group or owner

    }

    /**
     * @Rest\Post("/group/{id}/members", name="add_group_member")
     */
    public function addGroupMember()
    {
        // ToDo check if user is owner && check member is not the owner

    }

    /**
     * @Rest\Delete("/group/{id}/member/{user_id}", name="remove_group_member_user")
     */
    public function removeGroupMemberUser()
    {
        // ToDo check if current user is owner
    }

    /**
     * @Rest\Get("/group/{id}/messages", name="get_group_messages")
     * @Rest\QueryParam(name="page", description="Offset for message list (from new to old)")
     * @Rest\QueryParam(name="count", description="Count of messages")
     */
    public function getGroupMessages($id, ParamFetcher $paramFetcher)
    {
        $MAX_MESSAGES_PER_PAGE = 10;

        $id = (int) $id;
        $page = $paramFetcher->get('page');
        $count = $paramFetcher->get('count');

        if (!$page) {
            $page = 0;
        }
        if (!$count || $count > $MAX_MESSAGES_PER_PAGE) {
            $count = $MAX_MESSAGES_PER_PAGE;
        }
        $offset = $page * $count;

        if($id) {
            $messages = $this->messageRepository->createQueryBuilder('m')
                ->select('m')
                ->where('m.group = :group_id')
                ->orderBy('m.createdAt', 'DESC')
                ->setParameter('group_id', $id)
                ->setFirstResult($offset)
                ->setMaxResults($count)
                ->getQuery()->getResult();

            $result = [];

            // ToDo add createdBy field
            /* @var $message Message */
            foreach ($messages as $message) {
                $result[] = [
                    'id' => $message->getId(),
                    'type' => $message->getType(),
                    'content' => $message->getContent(),
                    'createdAt' => $message->getCreatedAt()
                ];
            }

            return $this->json([
                'data' => $result
            ], Response::HTTP_OK);
        }

        return $this->json([
            'message' => 'Invalid parameters'
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Post("/group/{id}/messages", name="add_message_to_group")
     * @Rest\RequestParam(name="message_type", description="Message type", nullable=false)
     * @Rest\RequestParam(name="message_content", description="Message content", nullable=false)
     */
    public function addMessage($id, ParamFetcher $paramFetcher)
    {
        $AVAILABLE_MESSAGE_TYPES = ['text'];
        $id = (int) $id;
        $message_type = trim($paramFetcher->get('message_type'));
        $message_content = htmlspecialchars(trim($paramFetcher->get('message_content')));

        // ToDo check if user is member of group
        if ($id && $message_content && in_array($message_type, $AVAILABLE_MESSAGE_TYPES)) {
            $group = $this->groupRepository->find($id);
            if($group) {
                $message = new Message();
                $message->setType($message_type);
                $message->setContent($message_content);
                $message->setCreatedAt(new \DateTime());
//                $message->setOwner();
                $group->addMessage($message);
                $this->entityManager->persist($message);
                $this->entityManager->flush();

                return $this->json(['success' => true], Response::HTTP_CREATED);
            }
        }

        return $this->json([
            'message' => 'Invalid parameters'
        ], Response::HTTP_BAD_REQUEST);
    }
}
