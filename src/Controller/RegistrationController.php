<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class RegistrationController
 * @package App\Controller
 * @Route("/api")
 */
class RegistrationController extends AbstractFOSRestController
{
    private $userRepository;
    private $passwordEncoder;
    private $entityManager;

    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager) {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    /**
     * @Rest\Post("/registration", name="registration")
     * @Rest\RequestParam(name="username", description="Username of user", nullable=false)
     * @Rest\RequestParam(name="password", description="Password of user", nullable=false)
     */
    public function registerUser(ParamFetcher $paramFetcher): Response
    {
        $username = htmlspecialchars($paramFetcher->get('username'));
        $password = $paramFetcher->get('password');

        if(!trim($username) || !trim($password)) {
            return $this->json([
                'message' => 'Invalid parameters'
            ], Response::HTTP_BAD_REQUEST);
        }

        $user = $this->userRepository->findOneBy([
            'username' => $username
        ]);

        if (!is_null($user)) {
            return $this->json([
                'message' => "User with username '$username' already exists"
            ], Response::HTTP_CONFLICT);
        }

        $user = new User();
        $user->setUsername($username);
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $password)
        );

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->json([
            'success' => true
        ], Response::HTTP_CREATED);
    }
}
