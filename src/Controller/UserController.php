<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/api")
 */
class UserController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/users", name="find_users")
     */
    public function findUsers()
    {

    }

    /**
     * @Rest\Get("/user/groups", name="get_user_groups")
     */
    public function getUserGroups()
    {

    }
}
