<?php
namespace App\Serializer;


use App\Entity\Message;

class CircularReferenceHandler {
    public function __invoke($object) {
        return $object->getId();
    }
}